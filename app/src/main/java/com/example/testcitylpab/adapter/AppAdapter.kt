package com.example.testcitylpab.adapter

import com.example.testcitylpab.model.EntryModel

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.testcitylpab.core.PresenterTopAppsContractMVPImpl


class AppAdapter(private val context: Context, private val entryArrayList: ArrayList<EntryModel>, presenterTopAppsContractMVPImplPar: PresenterTopAppsContractMVPImpl) :
        androidx.recyclerview.widget.RecyclerView.Adapter<AppViewHolder>() {



    private var presenterTopAppsContractMVPImpl : PresenterTopAppsContractMVPImpl? = presenterTopAppsContractMVPImplPar

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): AppViewHolder {

        val inflater = LayoutInflater.from(context)

        return AppViewHolder(inflater, parent, context, presenterTopAppsContractMVPImpl)

    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {

        val entryModel: EntryModel = entryArrayList[position]
        holder.bind(entryModel, position)


    }

    override fun getItemCount() = entryArrayList.size
}

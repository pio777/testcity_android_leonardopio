package com.example.testcitylpab.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testcitylpab.core.PresenterTopAppsContractMVPImpl
import com.example.testcitylpab.model.EntryModel
import com.example.testcitylpab.R


class AppViewHolder(inflater: LayoutInflater, parent: ViewGroup, context: Context, presenterTopAppsContractMVPImpl: PresenterTopAppsContractMVPImpl?) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.card_app_item, parent, false)) {


    private var mPresenterTopAppsContractMVPImpl: PresenterTopAppsContractMVPImpl? = presenterTopAppsContractMVPImpl

    private var cardViewApp: CardView? = null

    private var tvAppTitleCat: TextView? = null
    private var tvartistName: TextView? = null
    private var tvAppReleaseDate: TextView? = null
    private var tvAppLink: TextView? = null
    private var tvAppSummary: TextView? = null
    private var mYearView: TextView? = null

    private var ivAppPrevImage: ImageView


    init {
        cardViewApp = itemView.findViewById(R.id.cardVappDetail)

        tvAppTitleCat = itemView.findViewById(R.id.tvAppName)
        tvartistName = itemView.findViewById(R.id.tvAppArtist)
        tvAppReleaseDate = itemView.findViewById(R.id.tvDate)
        tvAppLink = itemView.findViewById(R.id.tvAppDetail)
        tvAppSummary = itemView.findViewById(R.id.tvAppSummary)

        ivAppPrevImage = itemView.findViewById(R.id.ivAppImage)
    }

    fun bind(entryModel: EntryModel, position: Int) {

        if (position % 2 != 0)
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.gray))
        else
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.white))

        Log.d("bind", "link : ${entryModel}")

        tvAppTitleCat?.text = String.format("%s (%s)", entryModel.title, entryModel.category)
        tvartistName?.text = entryModel.imArtist// String.format("%s ...", entryModel.imPrice.toString().take(100))
        tvAppReleaseDate?.text = entryModel.imReleaseDate
        tvAppLink?.text = entryModel.link
        tvAppSummary?.text = entryModel.summary


        cardViewApp?.setOnClickListener {
            entryModel.imImage
            Log.d("cardViewApp", "Do something..${position} on ${entryModel.title}")

            mPresenterTopAppsContractMVPImpl?.appDetail(position)
        }

        mYearView?.text = entryModel.imName


        Glide.with(itemView)
                .load(entryModel.imImage)
                .into(ivAppPrevImage)

    }

}


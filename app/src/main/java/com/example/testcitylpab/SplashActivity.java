package com.example.testcitylpab;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.ImageViewTarget;

public class SplashActivity extends AppCompatActivity {

    private Handler mDelayHandler = null;
    private Runnable mRunnable;
    private final Long SPLASH_DELAY = 3000L; //3 seconds
    private ImageView imVsplash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_view);
        imVsplash = findViewById(R.id.imgVsplash);


        Glide.with(imVsplash.getContext())
                .asGif()
                .load(R.drawable.apps_items)
                .placeholder(ResourcesCompat.getDrawable(getApplicationContext().getResources(),
                        R.drawable.apps_items, null))
                .centerCrop()
                .into(new ImageViewTarget<GifDrawable>(imVsplash) {
                    @Override
                    protected void setResource(@Nullable GifDrawable resource) {
                        imVsplash.setImageDrawable(resource);
                    }
                });


        Handler handler = new Handler();
        mRunnable = () -> {
            startActivity(new
                    Intent(SplashActivity.this, ViewTopAppsContractMVPImpl.class));
            //     overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        };
        handler.postDelayed(mRunnable, SPLASH_DELAY);

    }


    @Override
    protected void onDestroy() {
        if (mDelayHandler != null) {
            mDelayHandler.removeCallbacks(mRunnable);
        }
        super.onDestroy();

    }
}

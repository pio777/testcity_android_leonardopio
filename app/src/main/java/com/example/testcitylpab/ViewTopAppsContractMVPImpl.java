package com.example.testcitylpab;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.testcitylpab.adapter.AppAdapter;
import com.example.testcitylpab.core.TopAppsContractMVP;
import com.example.testcitylpab.core.PresenterTopAppsContractMVPImpl;
import com.example.testcitylpab.core.fragments.AppDetailFragment;
import com.example.testcitylpab.core.fragments.BaseFragment;
import com.example.testcitylpab.model.EntryModel;

import java.util.ArrayList;

public class ViewTopAppsContractMVPImpl extends AppCompatActivity implements TopAppsContractMVP.View {

    private PresenterTopAppsContractMVPImpl mPresenterTopAppsContractMVPImpl;
    private RecyclerView rvTopApps;
    private AppAdapter appAdapter;
    private ArrayList<EntryModel> entryModelArrayList;
    private FrameLayout frameLayoutcontain;

    private LinearLayoutManager linearLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPresenterTopAppsContractMVPImpl = new PresenterTopAppsContractMVPImpl(this);
        mPresenterTopAppsContractMVPImpl.getDataFromURL(getApplicationContext(), "");

        frameLayoutcontain = findViewById(R.id.fragment_container);
        rvTopApps = findViewById(R.id.rvTopApps);
        linearLayoutManager = new LinearLayoutManager(this);

        rvTopApps.setLayoutManager(linearLayoutManager);
        rvTopApps.setHasFixedSize(true);

    }


    @Override
    protected void onResume() {
        super.onResume();
        mPresenterTopAppsContractMVPImpl.getDataFromURL(getApplicationContext(), "");
    }

    @Override
    public void onGetDataSuccess(String message, ArrayList<EntryModel> entryModelArrayList) {
        this.entryModelArrayList = entryModelArrayList;
        appAdapter = new AppAdapter(getApplicationContext(), entryModelArrayList, mPresenterTopAppsContractMVPImpl);
        rvTopApps.setAdapter(appAdapter);
    }


    @Override
    public void viewAppDetail(int position) {

        Log.w("mGetDataView", "Do something" + entryModelArrayList.get(position));

        mPresenterTopAppsContractMVPImpl.addFragment(new AppDetailFragment(entryModelArrayList.get(position)));
    }


    @Override
    public void onBackPressed() {
        frameLayoutcontain.setVisibility(View.GONE);
        rvTopApps.setVisibility(View.VISIBLE);
    }


    @Override
    public void onGetDataFailure(String message) {
        Toast.makeText(this, "Get Data Failure :(", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNoInternetConnection(String message) {
        Toast.makeText(this, "No internet conection", Toast.LENGTH_LONG).show();
    }

    @Override
    public void setFragment(BaseFragment fragment) {
        fragment.atachPresenter(mPresenterTopAppsContractMVPImpl);

        frameLayoutcontain.setVisibility(View.VISIBLE);
        rvTopApps.setVisibility(View.GONE);


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}

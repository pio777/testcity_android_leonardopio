package com.example.testcitylpab.model;


import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

public class EntryModel {

    @SerializedName("im:name")
    @Expose
    private String imName;

    @SerializedName("im:image")
    @Expose
    private String imImage;

    @SerializedName("summary")
    @Expose
    private String summary;

    @SerializedName("im:price")
    @Expose
    private String imPrice;

    @SerializedName("im:contentType")
    @Expose
    private String imContentType;

    @SerializedName("rights")
    @Expose
    private String rights;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("link")
    @Expose
    private String link;

    private String linkPreview;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("im:artist")
    @Expose
    private String imArtist;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("im:releaseDate")
    @Expose
    private String imReleaseDate;


    public EntryModel(String imName, String imImage, String summary, String imPrice, String imContentType, String rights, String title, String link, String linkPreview, String id, String imArtist, String category, String imReleaseDate) {
        this.imName = imName;
        this.imImage = imImage;
        this.summary = summary;
        this.imPrice = imPrice;
        this.imContentType = imContentType;
        this.rights = rights;
        this.title = title;
        this.link = link;
        this.linkPreview = linkPreview;
        this.id = id;
        this.imArtist = imArtist;
        this.category = category;
        this.imReleaseDate = imReleaseDate;
    }

    public EntryModel() {
    }

    public String getImName() {
        return imName;
    }

    public void setImName(String imName) {
        this.imName = imName;
    }

    public String getImImage() {
        return imImage;
    }

    public void setImImage(String imImage) {
        this.imImage = imImage;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImPrice() {
        return imPrice;
    }

    public void setImPrice(String imPrice) {
        this.imPrice = imPrice;
    }

    public String getImContentType() {
        return imContentType;
    }

    public void setImContentType(String imContentType) {
        this.imContentType = imContentType;
    }

    public String getRights() {
        return rights;
    }

    public void setRights(String rights) {
        this.rights = rights;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLinkPreview() {
        return linkPreview;
    }

    public void setLinkPreview(String linkPreview) {
        this.linkPreview = linkPreview;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImArtist() {
        return imArtist;
    }

    public void setImArtist(String imArtist) {
        this.imArtist = imArtist;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImReleaseDate() {
        return imReleaseDate;
    }

    public void setImReleaseDate(String imReleaseDate) {
        this.imReleaseDate = imReleaseDate;
    }


    @Override
    public String toString() {
        return "EntryModel{" +
                "imName='" + imName + '\'' +
                ", imImage='" + imImage + '\'' +
                ", summary='" + summary + '\'' +
                ", imPrice='" + imPrice + '\'' +
                ", imContentType='" + imContentType + '\'' +
                ", rights='" + rights + '\'' +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", linkPreview='" + linkPreview + '\'' +
                ", id='" + id + '\'' +
                ", imArtist='" + imArtist + '\'' +
                ", category='" + category + '\'' +
                ", imReleaseDate='" + imReleaseDate + '\'' +
                '}';
    }

    public static class DataStateDeserializer implements JsonDeserializer<EntryModel> {

        @Override
        public EntryModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            EntryModel entryModel = new EntryModel();
            JsonObject jsonObject = json.getAsJsonObject();

            entryModel.setImName(jsonObject.getAsJsonObject("im:name").get("label").getAsString());
            entryModel.setImImage(jsonObject.getAsJsonArray("im:image").get(2).getAsJsonObject().get("label").getAsString());
            entryModel.setSummary(jsonObject.getAsJsonObject("summary").get("label").getAsString());
            entryModel.setImPrice(jsonObject.getAsJsonObject("im:price").get("label").getAsString());
            entryModel.setImContentType(jsonObject.getAsJsonObject("im:contentType").getAsJsonObject("attributes").get("label").getAsString());
            entryModel.setRights(jsonObject.getAsJsonObject("rights").get("label").getAsString());
            entryModel.setTitle(jsonObject.getAsJsonObject("title").get("label").getAsString());

            entryModel.setLink(jsonObject.getAsJsonArray("link").get(0).getAsJsonObject().get("attributes").getAsJsonObject().get("href").getAsString());
            entryModel.setLinkPreview(jsonObject.getAsJsonArray("link").get(1).getAsJsonObject().get("attributes").getAsJsonObject().get("href").getAsString());

            entryModel.setId(jsonObject.getAsJsonObject("id").getAsJsonObject("attributes").get("im:id").getAsString());
            entryModel.setImArtist(jsonObject.getAsJsonObject("im:artist").get("label").getAsString());
            entryModel.setCategory(jsonObject.getAsJsonObject("category").getAsJsonObject("attributes").get("term").getAsString());
            entryModel.setImReleaseDate(jsonObject.getAsJsonObject("im:releaseDate").getAsJsonObject("attributes").get("label").getAsString());

            Log.d("deserialize", entryModel.toString());

            return entryModel;
        }
    }

}

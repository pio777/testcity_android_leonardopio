package com.example.testcitylpab.model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class FeedAppsModel {

    private String author;

    @SerializedName("entry")
    @Expose
    private ArrayList<EntryModel> entryModelArrayLis;

    private String updated;
    private String rights;
    private String title;
    private String icon;
    private String link;
    private String id;

    public FeedAppsModel(String author, ArrayList<EntryModel> entryModelArrayLis, String updated, String rights, String title, String icon, String link, String id) {
        this.author = author;
        this.entryModelArrayLis = entryModelArrayLis;
        this.updated = updated;
        this.rights = rights;
        this.title = title;
        this.icon = icon;
        this.link = link;
        this.id = id;
    }

    public FeedAppsModel() {

    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ArrayList<EntryModel> getEntryModelArrayLis() {
        return entryModelArrayLis;
    }

    public void setEntryModelArrayLis(ArrayList<EntryModel> entryModelArrayLis) {
        this.entryModelArrayLis = entryModelArrayLis;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getRights() {
        return rights;
    }

    public void setRights(String rights) {
        this.rights = rights;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "FeedAppsModel{" +
                "author='" + author + '\'' +
                ", entryModelArrayLis=" + entryModelArrayLis +
                ", updated='" + updated + '\'' +
                ", rights='" + rights + '\'' +
                ", title='" + title + '\'' +
                ", icon='" + icon + '\'' +
                ", link='" + link + '\'' +
                ", id='" + id + '\'' +
                '}';
    }


    public static class DataStateDeserializer implements JsonDeserializer<FeedAppsModel> {

        @Override
        public FeedAppsModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            //FeedAppsModel feedAppsModel = new Gson().fromJson(json, FeedAppsModel.class);

            FeedAppsModel feedAppsModel = new FeedAppsModel();

            JsonObject jsonObject = json.getAsJsonObject();

            Type listEntryType = new TypeToken<ArrayList<EntryModel>>() {
            }.getType();


            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(EntryModel.class, new EntryModel.DataStateDeserializer())
                    .setLenient()
                    .create();

           /* GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(VkAudioAlbumsResponse.class, new VkAudioAlbumsResponseDeserializer());
            Gson gson = gsonBuilder.create();
            Response response = gson.fromJson(jsonString, Response.class);*/


            ArrayList<EntryModel> entryModelArrayLis = gson.fromJson(jsonObject.getAsJsonArray("entry"), listEntryType);
            feedAppsModel.setEntryModelArrayLis(entryModelArrayLis);


            /*if (jsonObject.has("data")) {
                JsonElement elem = jsonObject.get("data");
                if (elem != null && !elem.isJsonNull()) {
                    if(elem.isJsonPrimitive()){
                        //feedAppsModel.setMessage(elem.getAsString());
                    }else{*/

            feedAppsModel.setAuthor(jsonObject.getAsJsonObject("author").getAsJsonObject("name").get("label").getAsString());
            feedAppsModel.setUpdated(jsonObject.getAsJsonObject("updated").get("label").getAsString());
            feedAppsModel.setRights(jsonObject.getAsJsonObject("rights").get("label").getAsString());
            feedAppsModel.setTitle(jsonObject.getAsJsonObject("title").get("label").getAsString());
            feedAppsModel.setIcon(jsonObject.getAsJsonObject("icon").get("label").getAsString());
            feedAppsModel.setId(jsonObject.getAsJsonObject("id").get("label").getAsString());

            Log.d("deserialize", feedAppsModel.toString());

                       /* feedAppsModel.setLastname(elem.getAsJsonObject().get("lastname").getAsString());
                        feedAppsModel.setMobilenumber(elem.getAsJsonObject().get("mobilenumber").getAsString());
                        feedAppsModel.setEmailid(elem.getAsJsonObject().get("emailid").getAsString());
                        feedAppsModel.setTimezone(elem.getAsJsonObject().get("timezone").getAsString());*/
                  /*  }
                }
            }*/
            return feedAppsModel;
        }
    }


}

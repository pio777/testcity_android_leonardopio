package com.example.testcitylpab.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedRootModel {


    @SerializedName("feed")
    @Expose
    private FeedAppsModel feedAppsModel;

    public FeedRootModel(FeedAppsModel feedAppsModel) {
        this.feedAppsModel = feedAppsModel;
    }

    public FeedAppsModel getFeedAppsModel() {
        return feedAppsModel;
    }

    public void setFeedAppsModel(FeedAppsModel feedAppsModel) {
        this.feedAppsModel = feedAppsModel;
    }


    @Override
    public String toString() {
        return "FeedRootModel{" +
                "feedAppsModel=" + feedAppsModel +
                '}';
    }
}

package com.example.testcitylpab.core.fragments;


import com.example.testcitylpab.core.PresenterTopAppsContractMVPImpl;

public interface FragmentNavigation {
    interface View {
        void atachPresenter(PresenterTopAppsContractMVPImpl presenterTopAppsContractMVPImpl);
    }


}
package com.example.testcitylpab.core.fragments;

import androidx.fragment.app.Fragment;

import com.example.testcitylpab.core.PresenterTopAppsContractMVPImpl;


public abstract class BaseFragment extends Fragment implements FragmentNavigation.View {

    protected PresenterTopAppsContractMVPImpl navigationPresenterTopAppsContractMVPImpl;

    @Override
    public void atachPresenter(
            PresenterTopAppsContractMVPImpl presenterTopAppsContractMVPImpl) {
        navigationPresenterTopAppsContractMVPImpl = presenterTopAppsContractMVPImpl;
    }
}
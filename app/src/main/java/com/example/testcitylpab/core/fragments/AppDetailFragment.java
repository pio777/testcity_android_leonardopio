package com.example.testcitylpab.core.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.testcitylpab.R;
import com.example.testcitylpab.model.EntryModel;

public class AppDetailFragment extends BaseFragment {

    private TextView tvAppTitleCat;
    private TextView tvartistName;
    private TextView tvAppReleaseDate;
    private TextView tvAppLink;
    private TextView tvAppSummary;
    private TextView mYearView;

    private ImageView ivAppPrevImage;


    private EntryModel entryModel;

    public AppDetailFragment(EntryModel entryModel) {
        super();
        this.entryModel = entryModel;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_detail,
                container, false);

        tvAppTitleCat = view.findViewById(R.id.tvAppName);
        ivAppPrevImage = view.findViewById(R.id.ivAppImage);
        tvAppSummary = view.findViewById(R.id.tvAppSummary);
        tvartistName = view.findViewById(R.id.tvAppArtist);
        tvAppLink = view.findViewById(R.id.tvLink);
        tvAppReleaseDate = view.findViewById(R.id.tvDate);


        tvAppTitleCat.setText(entryModel.getImName());
        tvAppReleaseDate.setText(entryModel.getImReleaseDate());
        tvartistName.setText("By: ".concat(entryModel.getImArtist()));

        Glide.with(view)
                .load(entryModel.getLinkPreview())
                .into(ivAppPrevImage);


        tvAppSummary.setText(entryModel.getSummary());


        tvAppSummary.setMovementMethod(new ScrollingMovementMethod());


        tvAppLink.setText(entryModel.getLink());
        tvAppLink.setLinkTextColor(Color.parseColor("#2f6699"));


        Linkify.addLinks(tvAppLink, Linkify.WEB_URLS);


        Log.d("AppDetailFragment", "" + entryModel);
        return view;

    }


}

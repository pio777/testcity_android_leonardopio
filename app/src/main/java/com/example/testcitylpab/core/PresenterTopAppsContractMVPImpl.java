package com.example.testcitylpab.core;

import android.content.Context;
import android.util.Log;


import com.example.testcitylpab.core.fragments.BaseFragment;
import com.example.testcitylpab.model.EntryModel;

import java.util.ArrayList;

public class PresenterTopAppsContractMVPImpl implements TopAppsContractMVP.Presenter, TopAppsContractMVP.onGetDataListener {

    private TopAppsContractMVP.View mGetDataView;
    private InteractorTopAppsContractMVPImpl mIntractor;

    public PresenterTopAppsContractMVPImpl(TopAppsContractMVP.View mGetDataView) {
        this.mGetDataView = mGetDataView;
        mIntractor = new InteractorTopAppsContractMVPImpl(this);
    }

    @Override
    public void getDataFromURL(Context context, String url) {
        try {

            if (mIntractor.isInternetAvailable(context)) {
                mIntractor.retriveTopAQpps(context, url);
            } else {
                mGetDataView.onNoInternetConnection("");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void appDetail(int position) {

        mGetDataView.viewAppDetail(position);
    }

    @Override
    public void addFragment(BaseFragment fragment) {
        mGetDataView.setFragment(fragment);
    }

    @Override
    public void onSuccess(String message, ArrayList<EntryModel> allCountriesData) {
        Log.d("onSuccess", "sucess");
        mGetDataView.onGetDataSuccess(message, allCountriesData);
    }


    @Override
    public void onFailure(String message) {
        mGetDataView.onGetDataFailure(message);
    }
}

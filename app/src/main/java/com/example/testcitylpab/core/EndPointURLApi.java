package com.example.testcitylpab.core;


import com.example.testcitylpab.model.FeedRootModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface EndPointURLApi {


    //BuildConfig.FLAVOR
    @GET("/us/rss/topfreeapplications/limit={limitByEnviroment}/json")
    Call<FeedRootModel> getFeedTopApps(
            @Path("limitByEnviroment") String limitByEnviroment
    );

}

package com.example.testcitylpab.core;

import com.example.testcitylpab.BuildConfig;
import com.example.testcitylpab.model.EntryModel;
import com.example.testcitylpab.model.FeedAppsModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientFactory {

    public RetrofitClientFactory() {
    }

    public static InteractorTopAppsContractMVPImpl getRetrofitMediaContentPlatformClient() {
        return getRetrofitInstance().create(InteractorTopAppsContractMVPImpl.class);
    }


    public static Retrofit getRetrofitInstance() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(FeedAppsModel.class, new FeedAppsModel.DataStateDeserializer())
                .registerTypeAdapter(EntryModel.class, new EntryModel.DataStateDeserializer())
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
}


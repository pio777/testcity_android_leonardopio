package com.example.testcitylpab.core;

import android.content.Context;

import com.example.testcitylpab.core.fragments.BaseFragment;
import com.example.testcitylpab.model.EntryModel;

import java.util.ArrayList;

public interface TopAppsContractMVP {

    interface View {
        void onGetDataSuccess(String message, ArrayList<EntryModel> list);

        void viewAppDetail(int position);

        void setFragment(BaseFragment fragment);

        void onGetDataFailure(String message);

        void onNoInternetConnection(String message);
    }

    interface Presenter {
        void getDataFromURL(Context context, String url);

        void appDetail(int position);

        void addFragment(BaseFragment fragment);
    }

    interface Interactor {
        void retriveTopAQpps(Context context, String url);

    }

    interface onGetDataListener {
        void onSuccess(String message, ArrayList<EntryModel> list);

        void onFailure(String message);
    }
}

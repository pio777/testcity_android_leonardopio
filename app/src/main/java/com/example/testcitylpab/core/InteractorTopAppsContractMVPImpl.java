package com.example.testcitylpab.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.testcitylpab.BuildConfig;
import com.example.testcitylpab.model.FeedRootModel;


import retrofit2.Retrofit;

public class InteractorTopAppsContractMVPImpl implements TopAppsContractMVP.Interactor {

    private TopAppsContractMVP.onGetDataListener mOnGetDatalistener;

    public InteractorTopAppsContractMVPImpl(TopAppsContractMVP.onGetDataListener mOnGetDatalistener) {
        this.mOnGetDatalistener = mOnGetDatalistener;
    }

    public boolean isInternetAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void retriveTopAQpps(Context context, String url) {
        Retrofit retrofit = RetrofitClientFactory.getRetrofitInstance();

        EndPointURLApi request = retrofit.create(EndPointURLApi.class);

        retrofit2.Call<FeedRootModel> call = request.getFeedTopApps(
                BuildConfig.FLAVOR.equals("DEV") ? "10" : "20");

        call.enqueue(new retrofit2.Callback<FeedRootModel>() {
            @Override
            public void onResponse(retrofit2.Call<FeedRootModel> call, retrofit2.Response<FeedRootModel> response) {

                Log.i("onResponseBody", response.body().toString() + "" + call);

                FeedRootModel jsonResponse = response.body();
                Log.i("onResponse", jsonResponse.toString() + "" + call);

                mOnGetDatalistener.onSuccess("Apps Size: " + jsonResponse.getFeedAppsModel().getEntryModelArrayLis().size(), jsonResponse.getFeedAppsModel().getEntryModelArrayLis());

            }

            @Override
            public void onFailure(retrofit2.Call<FeedRootModel> call, Throwable t) {
                Log.v("Error", t.getMessage());
                mOnGetDatalistener.onFailure(t.getMessage());
            }
        });
    }
}
